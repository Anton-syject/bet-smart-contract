The Bet Smart Contract manages betting and guarantees that winners will be rewarded as long as a bet company add correct results of matches. 
The Contract was designed for football but it can be used or adopted to other kinds of sports.

The Bet Smart Contract provides next features:

Player:
Making a bet on winner or draw 
The contract holds fund and guarantee reward (this feature is in progress and will be soon)
Getting rewarded after publishing results


Administrator
Creating Sport  Events
Adding a final result to a certain event
Adding contract with coefs for a certain event
