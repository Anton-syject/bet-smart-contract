pragma solidity 0.5.1;


contract FootballEvents {
    address admin;
    constructor() public {
        admin = msg.sender;
    }
    modifier OnlyAdmin {
        require(msg.sender == admin);
        _;
    }
    
  
    //General info about a Football event
    struct Event {
        string League;
        string Team1;
        string Team2;
        uint startDate_Time;
        uint endDate_Time;
        bool isExist;
    }
    
    mapping (uint => Event) public events;
    
    function AddEvent(uint eventCode, string memory _league, string memory _team1, string memory _team2, uint _startDateAndTime, uint _endDateAndTime) public OnlyAdmin returns (bool success) {
        require(_startDateAndTime > now && _endDateAndTime > now && _endDateAndTime > _startDateAndTime);
        events[eventCode] = Event(_league, _team1, _team2, _startDateAndTime, _endDateAndTime, true) ;
        return true;
    }
    
    //Result section 
    struct Result {
        int Team1Score;
        int Team2Score;
        string winner;
        mapping (string => int) PlayersAndGoals;
    }
    
    mapping (uint => Result) public results;
    

    function AddResult(uint eventCode, int _team1score, int _team2score,  string memory _winner) public OnlyAdmin {
        require(events[eventCode].isExist == true);
        require(now > events[eventCode].endDate_Time);
        results[eventCode] = Result(_team1score, _team2score, _winner);
    }
    
    //Creating an EventBet
    EventBet[] public eventbets;
    function createEventBet(uint eventCode, int _team1win, int _team2win, int _draw) public OnlyAdmin{
        require(events[eventCode].isExist == true);
        EventBet FootballEvent = new EventBet(eventCode, _team1win, _team2win, _draw, events[eventCode].startDate_Time, events[eventCode].endDate_Time);
        eventbets.push(FootballEvent);
    }
    
    //Functions for external contract
    function getResult(uint eventCode) public view returns (int t1goals, int t2goals) {
        int x = results[eventCode].Team1Score;
        int y = results[eventCode].Team2Score;
        return (x, y);
    }
    

}


contract EventBet  {
  
    uint eventCode;
    FootballEvents parentContract;
    //Coef section
        int team1win;
        int team2win;
        int draw;
        uint startDate_Time;
        uint endDate_Time;
    
    modifier beforeStart {
        require(now < startDate_Time);
        _;
    }
    
    modifier afterEnd {
        require(now > endDate_Time);
        _;
    }
   
    constructor (uint _eventCode, int _team1win, int _team2win, int _draw, uint _startTime, uint _endTime) public {
        eventCode = _eventCode;
        team1win = _team1win;
        team2win = _team2win;
        draw = _draw;
        parentContract = FootballEvents(msg.sender);
        startDate_Time = _startTime;
        endDate_Time = _endTime;
    }
    
    //User make a bet section
    mapping (address => uint) betsOnTeam1;
    mapping (address => uint) betsOnTeam2;
    mapping (address => uint) betsOnDraw;
    mapping (address => bool) getRewarded; 
    
    
    function makeBetOnTeam1() payable public beforeStart {
        betsOnTeam1[msg.sender] = msg.value;
    }
    
    function makeBetOnTeam2() payable public beforeStart {
        betsOnTeam2[msg.sender] = msg.value;
    }
    
    function makeBetOnDraw() payable public beforeStart {
        betsOnDraw[msg.sender] = msg.value;
    }
    
    
    //Get reward section
    function checkReward() public afterEnd returns (int reward) {
       (int t1goals, int t2goals) = FootballEvents(parentContract).getResult(eventCode);
       if (t1goals > t2goals) {
           return int(betsOnTeam1[msg.sender]) * team1win / 1000;
       }else if (t1goals < t2goals) {
           return int(betsOnTeam2[msg.sender]) * team2win / 1000;
       }else if (t1goals == t2goals) {
           return int(betsOnDraw[msg.sender]) * draw / 1000;
       }
       
    }
    
    function getReward() public afterEnd {
        require(getRewarded[msg.sender] == false);
        int reward;
       (int t1goals, int t2goals) = FootballEvents(parentContract).getResult(eventCode);
       if (t1goals > t2goals) {
           reward = int(betsOnTeam1[msg.sender]) * team1win / 1000;
           msg.sender.transfer(uint(reward));
           getRewarded[msg.sender] = true;
           
       }else if (t1goals < t2goals) {
           reward = int(betsOnTeam2[msg.sender]) * team2win / 1000;
           msg.sender.transfer(uint(reward));
           getRewarded[msg.sender] = true;
       }else if (t1goals == t2goals) {
           reward = int(betsOnDraw[msg.sender]) * draw / 1000;
           msg.sender.transfer(uint(reward));
           getRewarded[msg.sender] = true;
           
       }
       
       
    }

    function() external payable {}

   
}